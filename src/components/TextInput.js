import React,{Component} from 'react';
import { TextInput,StyleSheet,Dimensions } from 'react-native';

export default TetxtInput = (props) =>{
    const [value, onChangeText] = React.useState(props.ans);
    return(
        <TextInput style={styles.text}
            value={value}
            onChangeText={(value)=>onChangeText(value)}/>
    )
}

const styles = StyleSheet.create({
    text:{
        width : Dimensions.get('window').width-20,
        backgroundColor : '#a1735a',
        marginHorizontal:10,
        padding:10,
        color : 'white',
        fontSize:20
    }
    
})