import React,{Component} from 'react';
import { TouchableHighlight,StyleSheet ,Text, Dimensions,View} from 'react-native';

export default class Button extends Component{
    _handleOnPress = (value) => {
        this.props.onPress(value);
        
    }
    render(){
        return(
            <View style={styles.buttonContainer}>
                {
                    this.props.buttons.map((row, index) => (
                        <View key={index} style={styles.contRow}>
                            { 
                                row.map((col,index) => (
                                    <TouchableHighlight
                                        key={index}
                                        onPress={() => this._handleOnPress(col)}
                                        style={(index != 3)?styles.btnContainer:styles.btn}
                                        //background={TouchableNativeFeedback.SelectableBackground()}
                                        >
                                        <View style={styles.contButton}>
                                            <Text style={styles.txtDefault}>{col}</Text>
                                        </View>
                                    </TouchableHighlight>
                                ))
                            }
                        </View>
                    ))
                }
            </View>
        )
    }
   
}

const styles = StyleSheet.create({
    btnContainer : {
        width : Dimensions.get('window').width / 4,
        //height : Dimensions.get('window').height / 5,
        height : 80,
        //backgroundColor:'red',
        justifyContent:'center',
        borderWidth:1,
       // margin : 10
    },
    btn:{
        width : Dimensions.get('window').width / 4,
        //height : Dimensions.get('window').height / 5,
        height : 80,
        backgroundColor:'red',
        justifyContent:'center',
        borderWidth:1,
       // margin : 10
    },
    // txt:{
    //     fontSize:30,
    //     fontWeight:"700",
    //     textAlign : 'center',

    // },
    buttonContainer: {
        flex: 1,
        width: '100%',
        position: 'absolute', //Here is the trick
        bottom: 0, //Here is the trick

    },
    // row: {
    //     flexDirection: 'row'
    // }
    container: {
        flex:1,
      },
    
      txtDefault: {
        color: '#000',
        fontFamily: 'Helvetica-Light',
        fontSize: 20
      },
    
      contRow: {
        flex: 1,
        flexDirection: 'row'
      },
    
      contButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: '#ecf0f1'
      }
})