import React, { Component } from 'react';
import { View, Text, StyleSheet,TouchableHighlight,TouchableOpacity,Dimensions,TextInput, StatusBar} from 'react-native';

const buttons = [
    ['CLEAR','÷'],
    ['7', '8', '9', '-'],
    ['4', '5', '6', '+'],
    ['1', '2', '3', '='],
]
const init = '0';
let count = 0;
export default class Demo extends Component{
    state={
        _output:init
    }

    _handleOnPress(value){
        console.log(this.state._output)
        if(this.state._output == init && (value == "+" || value == "=" || value == "÷")){
            this.setState({_output:init})
        }else if(value == "CLEAR"){
            this.setState({_output:init})
            count = 0
        }else if(value == "="){
            this._evaluate();
        }else if(value == "+" || value == "=" || value == "÷" || value == "+" || value == "-"){
            
            if(count == 0){
                this._concate(value);
                count = count+1;
            }
        }
        else{
            this._concate(value);   
        }

        // switch(value){
        //     case "CLEAR":
        //         this.setState({_output:init})
        //     case "=":
        //         this._evaluate();
        //         break;
        //     default:
        //         this._concate(value);
        //         break;
        // }
    }

    _concate(value){
        console.log("count",count)
        if(this.state._output !== init){
            this.setState({ _output: this.state._output + '' + value + '' })
            count = 0;
        }
        else {
            this.setState({ _output: value + '' })
        }
    }

    _evaluate(){
        try {
            let strCurOutput = this.state._output;
            if (isNaN(strCurOutput)) {
                console.log("evaluate =>",this.state._output)
                let dEval = eval(this._convertToMathExpression(this.state._output));
                console.log("dEval",dEval)
                
                if(dEval === Infinity){
                    alert("Invalid");
                    this.setState({
                        _output: init,
                    })
                }else{
                    this.setState({
                        _output: '' + dEval,
                    })
                }
                
                
            }
        }
        catch (exception) {
           //alert("Invalid")
           console.log(exception)
        }
    }

    _convertToMathExpression = (value) => {
        console.log("_convertToMathExpression =>",value)
        let strTemp = value.replace(new RegExp(this._escapeRegExp(buttons[0][1]), 'g'), '/');
       // strTemp = strTemp.replace(new RegExp(this._escapeRegExp(buttons[1][3]), 'g'), '*');
        return strTemp;
    }

    _escapeRegExp = (str) => {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    }

    render(){
        return(
            <View style={{ flex: 1 }}>
                <StatusBar barStyle="light-content"/>
                <View style={styles.buttonContainer}>
                    <View style={styles.display}>
                    </View>
                    <Text style={styles.text} >{this.state._output}</Text>
                    {
                    buttons.map((row, index) => (
                        
                        <View key={index} style={styles.contRow}>
                            
                            { 
                                row.map((col,index) => (
                                    
                                    <TouchableHighlight
                                        key={index}
                                        onPress={() => this._handleOnPress(col)}
                                        style={[col == "CLEAR"?styles.clearContainer:(index == 3 || col == "÷")?styles.btn:styles.btnContainer]}
                                        //style={(index != 3)?styles.btnContainer:styles.btn}
                                        //background={TouchableNativeFeedback.SelectableBackground()}
                                        >
                                        <View style={styles.contButton}>
                                            <Text style={styles.txtDefault}>{col}</Text>
                                        </View>
                                    </TouchableHighlight>
                                ))
                            } 
                            
                            
                        </View>
                    ))
                }
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        flex: 1,
        width: '100%',
        position: 'absolute',
        bottom: 0, 
    },
    display:{
       // height : Dimensions.get('window').height - 320
       backgroundColor : '#a1735a',
       height:Dimensions.get('window').height
    },
    row: {
        flexDirection: 'row'
    },
    clearContainer:{
        width : Dimensions.get('window').width-Dimensions.get('window').width / 4,
        height : 80,
        justifyContent:'center',
        borderWidth:1,
    },
    btnContainer : {
        width : Dimensions.get('window').width / 4,
        height : 80,
        justifyContent:'center',
        borderWidth:1,
    },
    btn:{
        width : Dimensions.get('window').width / 4,
        height : 80,
        backgroundColor:'red',
        justifyContent:'center',
        borderWidth:1,
    },
    text:{
        width : Dimensions.get('window').width,
        backgroundColor : '#a1735a',
        padding:10,
        color : 'white',
        fontSize:30,
        textAlign:'right',
    },
    buttonContainer: {
        flex: 1,
        width: '100%',
        position: 'absolute', //Here is the trick
        bottom: 0, //Here is the trick

    },
    // row: {
    //     flexDirection: 'row'
    // }
    container: {
        flex:1,
      },
    
      txtDefault: {
        color: '#000',
        fontFamily: 'Helvetica-Light',
        fontSize: 25,
        fontWeight:"700"
      },
    
      contRow: {
        flex: 1,
        flexDirection: 'row'
      },
    
      contButton: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 0.5,
        borderColor: '#ecf0f1'
      }
})