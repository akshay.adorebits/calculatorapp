import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Button from '../components/Button';
import TextInput from '../components/TextInput';

const buttons = [
    ['CLEAR'],
    ['7', '8', '9', '÷'],
    ['4', '5', '6', 'x'],
    ['1', '2', '3', '+'],
    ['.', '0', '=', '-']
]
const initialOutput = '0';
const maxLength = 57;
export default class CalculatorScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            _output: initialOutput,
            _mathExpression: '',
            _history: [],
        }

    }

    _handleEvent = (value) => {

        if (!isNaN(value) || value == '.') {
            this._concatToOutput(value);
            console.log(value)
        }
        else {
            console.log("value in handel",this.state._output)
            switch (value) {

                case buttons[0][0]:
                    this._initOutput();
                    break;

                case buttons[4][2]:
                    this._evaluate();
                    break;

                default:
                    this._concatToOutput(value);
                    break;
            }
        }
    }

    _concatToOutput = (value) => {
        if (this.state._output.length >= maxLength) {
            this._showToast('Maximum Expression Length of ' + maxLength + ' is reached.');
        }
        else {
            console.log("init ==>",this.state._output)
            if (this.state._output !== initialOutput) {
                console.log('data ===>',this.state._output + '' + value + '')
                this.setState({ _output: this.state._output + '' + value + '' })
                console.log("sho output ==>",this.state._output)
            }
            else {
                this.setState({ _output: value + '' })
            }
        }
    }

    _evaluate = () => {
        try {
            let strCurOutput = this.state._output;
            if (isNaN(strCurOutput)) {
                console.log("evaluate =>",this.state._output)
                let dEval = eval(this._convertToMathExpression(this.state._output));
                console.log("dEval",dEval)
                
                this.setState({
                    _output: '' + dEval,
                })  
            }
        }
        catch (exception) {
           alert("Invalid")
        }
    }

    _convertToMathExpression = (value) => {
        
        let strTemp = value.replace(new RegExp(this._escapeRegExp(buttons[1][3]), 'g'), '/');
        strTemp = strTemp.replace(new RegExp(this._escapeRegExp(buttons[2][3]), 'g'), '*');
        console.log("_convertToMathExpression =>",value)
        return strTemp;
    }

    _escapeRegExp = (str) => {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
      }
    
      //Function to initialize output state
      _initOutput = () => {
        this.setState({
          _output: initialOutput
        })
        console.log("init = > ", this.state._output)
      }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <TextInput ans={this.state._output} />
                <View style={styles.buttonContainer}>
                    {/* <Button onClick={this.onClick}/> */}
                    <Button buttons={buttons} onPress={(e) => this._handleEvent(e)} />
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    buttonContainer: {
        flex: 1,
        width: '100%',
        position: 'absolute', //Here is the trick
        bottom: 0, //Here is the trick

    },
    row: {
        flexDirection: 'row'
    }
})