import React,{Component} from 'react';
import {SafeAreaView,Text} from 'react-native';
import CalculatorScreen from './src/screens/CalculatorScreen';
import Demo from './src/screens/Demo'

export default class App extends Component{
  render(){
    return(
      <SafeAreaView style={{flex:1}}>
        {/* <CalculatorScreen /> */}
        <Demo />
      </SafeAreaView>
    )
  }
}